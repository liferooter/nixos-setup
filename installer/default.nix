{ modulesPath
, hostname
, pkgs
, inputs
, self
, serverConfig
, ...
}:
{
  imports = [
    (modulesPath + "/installer/cd-dvd/installation-cd-minimal.nix")
    ./network.nix
    ./zram.nix
  ];

  environment.systemPackages = [
    (pkgs.writeShellApplication {
      name = "install-nixos";

      runtimeInputs = [
        inputs.disko.packages.${pkgs.system}.disko-install
      ];

      text =
        let
          mainDisk = serverConfig.disko.devices.disk.main.device;
        in
        ''
          sudo disko-install --flake ${self}#${hostname} --disk main ${mainDisk}
        '';
    })
  ];
}
