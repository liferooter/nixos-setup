{ serverConfig, ... }:
let
  cfg = serverConfig.setup.server.network;
in
{
  systemd.network = {
    enable = true;

    networks.main = {
      matchConfig.Name = cfg.interface;
      address = [ "${cfg.address}/32" ];
      dns = [ "1.1.1.1" ];
      routes = [{
        Gateway = cfg.gateway;
        GatewayOnLink = true;
      }];
    };
  };
  networking.useDHCP = false;
}
