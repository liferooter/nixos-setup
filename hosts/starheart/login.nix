{ lib, pkgs, ... }:
{
  services.greetd =
    {
      enable = true;
      settings = {
        default_session.command =
          "${lib.getExe pkgs.greetd.tuigreet} --cmd 'default-user-session'";
        initial_session = {
          command = "default-user-session";
          user = "liferooter";
        };
      };
    };
  security.pam.services = {
    swaylock = { };
    gtklock = { };
    hyprlock = { };
  };
}
