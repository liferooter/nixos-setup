{ pkgs
, self
, ...
}:
{
  fonts = {
    fontconfig.enable = true;
    googleFonts = [ ];
    nerdFonts = [
      "JetBrainsMono"
    ];
    preferred = {
      monospace = "JetBrainsMono Nerd Font";
      sansSerif = "Inter";
      useFontconfig = true;
    };
  };

  fonts.packages =
    (with self.packages.${pkgs.system}; [
      hse-sans
      hse-slab
    ]) ++ (with pkgs; [
      inter
    ]);
}
