{ inputs, pkgs, ... }: {
  imports = [
    inputs.nix-index-database.nixosModules.nix-index

    ./hardware.nix
    ./login.nix
    ./portals.nix
    ./fonts.nix
  ];

  setup.system = "x86_64-linux";

  setup.keymap = {
    colemak.enable = true;
    useDefaultTweaks = true;
  };

  services.logiops.enable = true;
  virtualisation.podman.enable = true;
  virtualisation.containerd.enable = true;

  catppuccin.enable = true;

  users.defaultUserShell = pkgs.fish;

  setup.computerType = "laptop";
  system.stateVersion = "24.05";
}
