{ pkgs
, ...
}:
{
  services.gnome.gnome-keyring = {
    enable = true;
  };
  services.dbus.packages = with pkgs; [
    gcr
  ];
  xdg.portal = {
    extraPortals = with pkgs; [
      xdg-desktop-portal-gtk
      xdg-desktop-portal-wlr
      xdg-desktop-portal-gnome
      gnome-keyring
    ];
    config.sway = {
      default = [
        "gtk"
        "wlr"
      ];
      "org.freedesktop.impl.portal.Secret" = [
        "gnome-keyring"
      ];
    };
    config.niri = {
      default = [
        "gnome"
        "gtk"
      ];
      "org.freedesktop.impl.portal.Secret" = [
        "gnome-keyring"
      ];
    };
  };
}
