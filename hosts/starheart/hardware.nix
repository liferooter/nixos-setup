{ ... }:
{
  boot.initrd.availableKernelModules = [
    "nvme"
    "sdhci_pci"
  ];
  boot.kernelModules = [ "kvm-amd" ];

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/ebd916f5-3e1c-4c21-84de-003bf4d258f3";
    fsType = "btrfs";
    options = [ "subvol=nixos,compress=zstd:1,discard=async" ];
  };

  boot.initrd.luks.devices.cryptroot = {
    device = "/dev/disk/by-uuid/7c4eeccb-06e8-417a-9702-02fefe19152e";
  };

  swapDevices = [ ];

  hardware.enableRedistributableFirmware = true;
  hardware.cpu.amd.updateMicrocode = true;
  hardware.amdgpu.initrd.enable = true;
}
