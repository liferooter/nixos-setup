{ ... }:
let
  minecraftPort = 25565;
  voiceChatPort = 24454;
  mcData = "/var/lib/minecraft";
in
{
  systemd.tmpfiles.settings."minecraft".${mcData}.d = {
    user = "1000";
    group = "1000";
  };
  virtualisation.oci-containers = {
    containers.minecraft-server = {
      image = "itzg/minecraft-server:java17-graalvm";
      ports =
        let
          mcPort = toString minecraftPort;
          vcPort = toString voiceChatPort;
        in
        [
          "${mcPort}:${mcPort}"
          "${vcPort}:${vcPort}/udp"
        ];
      volumes = [
        "${mcData}:/data"
      ];
      workdir = "/data";
      extraOptions = [
        "-it"
      ];
      environment = {
        EULA = "true";
        TYPE = "FABRIC";
        VERSION = "1.20.1";
        FABRIC_VERSION = "0.16.9";
        TZ = "Europe/Moscow";
        USE_AIKAR_FLAGS = "true";
        MEMORY = "4G";
        JVM_XX_OPTS = "-javaagent:authlib-injector-1.2.5.jar=mc-auth.vanutp.dev";
      };
    };
  };

  networking.firewall = {
    allowedTCPPorts = [ minecraftPort ];
    allowedUDPPorts = [ voiceChatPort ];
  };
}
