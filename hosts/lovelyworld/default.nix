{ pkgs, ... }:
{
  imports = [
    ./hardware.nix
    # ./minecraft.nix
    # ./backups.nix
    ./modpack-server.nix
  ];

  setup.system = "x86_64-linux";

  users.defaultUserShell = pkgs.fish;
  setup.server.network.address = "149.154.64.112";

  setup.computerType = "server";
  system.stateVersion = "24.11";
}
