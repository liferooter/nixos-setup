{ config
, pkgs
, lib
, ...
}:
let
  mcDir = "/var/www/minecraft";
  githubSshKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIG4X1S9Jw0S6JZthfSAY5+qNVxzYhkzW7jG4/sNqJBeC actions@github";
in
{
  systemd.tmpfiles.settings."42-minecraft" = lib.genAttrs [
    "${mcDir}"
    "${mcDir}/launcher"
    "${mcDir}/data"
  ]
    (_: {
      d = {
        user = "modpack-dev";
        group = "users";
        mode = "0755";
      };
    });
  services.nginx = {
    enable = true;
    validateConfigFile = false;
    virtualHosts."mc.liferooter.dev" = {
      addSSL = true;
      enableACME = true;
      locations."= /" = {
        root = pkgs.writeTextDir "index.html" (lib.readFile ./launcher-index.html);
        tryFiles = "/index.html =404";
      };
      locations."/" = {
        index = "index.html";
        root = mcDir;
      };
    };
  };
  users.users.modpack-dev = {
    isNormalUser = true;
    openssh.authorizedKeys.keys = [
      config.setup.server.ssh.publicKey
      githubSshKey
    ];
  };
  networking.firewall.allowedTCPPorts = [ 80 443 ];
}
