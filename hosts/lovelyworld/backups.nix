{ pkgs
, lib
, ...
}:
{
  systemd.services.backup-minecraft = {
    serviceConfig.ExecStart = lib.getExe (pkgs.writeShellApplication {
      name = "backup-minecraft";

      runtimeInputs = with pkgs; [
        rdiff-backup
      ];

      text = ''
        mkdir -p /var/backups
        rdiff-backup /var/lib/minecraft \
                     /var/backups
        rdiff-backup --remove-older-than 7D --force /var/backups
      '';
    });
  };
  systemd.timers.backup-minecraft = {
    timerConfig = {
      OnCalendar = "*-*-* *:00:00";
      Unit = "backup-minecraft.service";
    };
    wantedBy = [ "default.target" ];
  };
}
