{ pkgs, lib, inputs, config, ... }:
let
  templateUsers = {
    bimbo = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILDs5QuFw2ReuXqd/OASTmpbRo0xYgilyekjNkmUOm/T marisido13@MacBook-Air-Masenka.local";
    alexander = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIZnC5js/BZvuu9hCQiGFcrNSYEljHH+yvSYzdDssWPD alexander@gonduras";
    kramink = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIENEQpQKZSj1qkmhgeWUHsLyAyKZPBSXzXZ2T4aY6kES krami@LAPTOP-H224E6VJ";
  };
in
lib.mkMerge ([
  {
    users.users.daryapvlv = {
      isNormalUser = true;
      shell = pkgs.fish;
      openssh.authorizedKeys.keys = [
        config.setup.server.ssh.publicKey
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIK9zFM/7jz2nPvHQM8WW3GbAcKl4unlSxK5j0IoAvzL4 darapavlova@MacBook-Pro-Dara.local"
      ];
    };
  }
  {
    users.users.bimbo.packages = with pkgs; [
      git asciinema micro glow
    ];
    home-manager.users.bimbo = {
      home.sessionVariables = {
        EDITOR = lib.mkForce "nano";
      };
    };
  }
] ++ lib.mapAttrsToList
  (username: sshKey: {
    users.users.${username} = {
      isNormalUser = true;
      shell = pkgs.fish;
      openssh.authorizedKeys.keys = [
        config.setup.server.ssh.publicKey
        sshKey
      ];
    };

    home-manager.users.${username} = {
      imports = [
        inputs.vscode-server.homeModules.default
      ];

      home.packages = with pkgs; [
        gcc
        gdb
        clang-tools
      ];

      programs.fish.enable = true;
      programs.starship.enable = true;
      programs.eza.enable = true;
      services.vscode-server.enable = true;

      home.stateVersion = "24.05";
    };
  })
  templateUsers)
