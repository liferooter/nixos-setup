{ inputs, pkgs, lib, config, ... }:
let
  comment-keeper = inputs.comment-keeper.packages.${pkgs.system}.default;
in
{
  users.users.comment-keeper = {
    isNormalUser = true;
    packages = [ comment-keeper ];
  };
  systemd.services.comment-keeper = {
    serviceConfig.ExecStart = lib.getExe comment-keeper;
    serviceConfig.User = config.users.users.comment-keeper.name;
    serviceConfig.EnvironmentFile = config.sops.templates."comment-keeper.env".path;
    wantedBy = [ "default.target" ];
  };
  sops.secrets.comment_keeper_token = {
    owner = config.users.users.comment-keeper.name;
    restartUnits = [ "comment-keeper.service" ];
  };
  sops.templates."comment-keeper.env" = {
    content = ''
      TELOXIDE_TOKEN=${config.sops.placeholder.comment_keeper_token}
    '';
    owner = config.users.users.comment-keeper.name;
  };
}
