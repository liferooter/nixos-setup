{ config, pkgs, ... }:
let
  iptables = "${pkgs.iptables}/bin/iptables";
  wgPort = 51820;
in
{
  networking.nat.enable = true;
  networking.firewall.allowedUDPPorts = [ wgPort ];
  sops.secrets.wireguard_key = {
    owner = "root";
    mode = "0400";
  };
  networking.wg-quick.interfaces = {
    wg0 = {
      address = [
        "10.200.200.1/24"
      ];
      autostart = true;
      listenPort = wgPort;

      # Public key: xjT3bxbeCAobw8zHHInCS2XKunH7erY7XOiJSd3BB2c=
      privateKeyFile = config.sops.secrets.wireguard_key.path;

      postUp = "${iptables} -A FORWARD -i %i -j ACCEPT; ${iptables} -A FORWARD -o %i -j ACCEPT; ${iptables} -t nat -A POSTROUTING -o ens3 -j MASQUERADE";
      postDown = "${iptables} -D FORWARD -i %i -j ACCEPT; ${iptables} -D FORWARD -o %i -j ACCEPT; ${iptables} -t nat -D POSTROUTING -o ens3 -j MASQUERADE";

      peers = [
        {
          # Teapot
          allowedIPs = [ "10.200.200.2/32" ];
          publicKey = "YYXfB7gqfbX4DKOeowjm4RPWMFD2UJGLedhmK/6pglI=";
        }
        {
          # Me
          allowedIPs = [ "10.200.200.3/32" ];
          publicKey = "jndw8XoULSO2GOmjbU7R8NNiDYqxYJelGhhArtjdXEA=";
        }
        {
          # Alex
          allowedIPs = [ "10.200.200.4/32" ];
          publicKey = "C/1XaIfCddPKIEANBYXXAFd1sfELPeafk4PvfFZvFD8=";
        }
      ];
    };
  };
}
