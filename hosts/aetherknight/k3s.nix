{ config, self, ... }:
{
  networking.firewall.allowedTCPPorts = [ 6443 2379 2380 10250 80 443 8443 ];
  networking.firewall.allowedUDPPorts = [ 8472 51820 443 ];
  sops.secrets.k3sToken = {
    sopsFile = "${self}/secrets/kubernetes.yml";
  };
  services.k3s = {
    enable = true;
    role = "server";
    extraFlags = [ "--disable=traefik" ];
    tokenFile = config.sops.secrets.k3sToken.path;
  };
}
