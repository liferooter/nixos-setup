{ inputs, pkgs, pkgs-unstable, lib, config, ... }:
let
  inherit (pkgs) system;
  tgpy = inputs.tgpy.packages.${system}.tgpy.overridePythonAttrs (old: {
    dependencies = old.dependencies ++ (with pkgs-unstable.python3Packages; [
      pytz
    ]);
  });
in
{
  users.users.tgpy = {
    isNormalUser = true;
    packages = [ tgpy pkgs.git ];
    openssh.authorizedKeys.keys = [ config.setup.server.ssh.publicKey ];
  };
  systemd.services.tgpy = {
    serviceConfig.ExecStart = "${tgpy}/bin/tgpy";
    serviceConfig.User = config.users.users.tgpy.name;
    wantedBy = [ "default.target" ];
  };
  system.activationScripts.ensure-modules-repo = {
    deps = [ "users" ];
    text =
      let
        postUpdate = pkgs.writeShellApplication {
          name = "post-update";
          runtimeInputs = with pkgs; [
            git
            systemd
          ];
          text = ''
            modules_dir="/home/tgpy/.config/tgpy/modules"

            rm -rf $modules_dir
            git clone --depth 1 --single-branch /home/tgpy/modules file://$modules_dir
            systemctl restart tgpy.service
          '';
        };
      in
      ''
        if [ ! -e /home/tgpy/modules ]; then
          mkdir /home/tgpy/modules
          cd /home/tgpy/modules
          ${lib.getExe pkgs.git} init --bare --initial-branch main
          chown -R tgpy:users /home/tgpy/modules
        fi
        ln -sf ${lib.getExe postUpdate} /home/tgpy/modules/hooks/post-update
      '';
  };
  systemd.serviceOwners.tgpy = "tgpy";
}
