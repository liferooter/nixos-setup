{ inputs, pkgs, lib, config, ... }:
let
  d-bot = (import inputs.d-bot { inherit pkgs; }).package;
in
{
  users.users.d-bot = {
    isNormalUser = true;
    packages = [ d-bot ];
  };
  systemd.services.d-bot = {
    serviceConfig.ExecStart = lib.getExe d-bot;
    serviceConfig.User = config.users.users.d-bot.name;
    serviceConfig.EnvironmentFile = config.sops.templates."dbot.env".path;
    wantedBy = [ "default.target" ];
  };
  sops.secrets.dbot_token = {
    owner = config.users.users.d-bot.name;
    restartUnits = [ "d-bot.service" ];
  };
  sops.templates."dbot.env" = {
    content = ''
      BOT_TOKEN=${config.sops.placeholder.dbot_token}
    '';
    owner = config.users.users.d-bot.name;
  };
}
