{ ... }:
{
  networking.firewall.allowedTCPPorts = [ 80 443 ];
  services.nginx = {
    enable = true;
    virtualHosts."liferooter.dev" = {
      addSSL = true;
      enableACME = true;
      default = true;
    };
  };
}
