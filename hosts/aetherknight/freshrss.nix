{ config, ... }:
{
  portServices = [ "freshrss" ];
  virtualisation.oci-containers.containers.freshrss = {
    image = "freshrss/freshrss";
    ports = [
      "${config.ports.freshrss.str}:80/tcp"
    ];
    volumes = [
      "/var/lib/freshrss:/var/www/FreshRSS/data"
    ];
    environment = {
      TZ = "Europe/Moscow";
      CRON_MIN = "*/20";
    };
  };
  services.nginx.virtualHosts."rss.liferooter.dev" = {
    addSSL = true;
    enableACME = true;
    locations = {
      "/".proxyPass = "http://127.0.0.1:${toString config.ports.freshrss.str}/";
    };
  };
}
