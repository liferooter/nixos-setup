{ pkgs, ... }:
{
  imports = [
    ./disks.nix
    # ./nginx.nix
    ./extra-users.nix
    ./tgpy.nix
    ./d-bot.nix
    ./comment-keeper.nix
    # ./freshrss.nix
    ./wireguard.nix
    ./k3s.nix
  ];

  setup.system = "x86_64-linux";

  users.users = {
    root.shell = pkgs.fish;
    liferooter.shell = pkgs.fish;
  };
  setup.server.network.address = "185.246.66.138";

  environment.systemPackages = [
    pkgs.zellij
  ];

  setup.computerType = "server";
  system.stateVersion = "24.05";
}
