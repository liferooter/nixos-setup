{ lib, pkgs, config, ... }:
{
  options = {
    wallpaper.file = lib.mkOption {
      description = "Default wallpaper file";
      type = with lib.types; nullOr path;
      default = null;
    };
  };
  config = lib.mkIf (config.wallpaper.file != null) {
    sass.includePath = [
      (pkgs.writeTextFile {
        name = "wallpaper-sass";
        text = ''
          $wallpaperUrl: url("${config.wallpaper.file}")
        '';
        destination = "/_wallpaper.scss";
      })
    ];
  };
}
