{ lib, config, ... }:
let
  genConfig =
    with lib;
    flip pipe [
      (mapAttrsToList (key: value: "${key} ${value}"))
      concatLines
    ];
in
{
  config = lib.mkMerge [
    {
      programs.less.enable = lib.mkDefault true;
    }
    (lib.mkIf config.setup.keymap.colemak.enable {
      programs.less.keys = genConfig {
        j = "repeat-search";
        J = "reverse-search";
        e = "forw-line";
        E = "forw-line-force";
        i = "back-line";
        I = "back-line-force";
      };
    })
  ];
}
