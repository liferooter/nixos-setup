{ config, lib, ... }:
{
  config = lib.mkIf config.setup.isLaptop {
    i18n.defaultLocale = "ru_RU.UTF-8";
  };
}
