{ lib, config, ... }: {
  services.udisks2.enable = lib.mkDefault config.setup.isLaptop;
}
