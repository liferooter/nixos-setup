{ lib, config, ... }:
{
  options = {
    util = lib.mkOption {
      description = "Utility functions passed via `utils` argument";
      type = with lib.types; attrsOf anything;
      default = { };
    };
  };
  config = {
    _module.args.util = config.util;
  };
}
