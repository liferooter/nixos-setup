{ ... }:
{
  config.programs.git = {
    userName = "Gleb Smirnov";
    userEmail = "glebsmirnov0708@gmail.com";
    ignores = [
      "/.direnv/"
      "/.helix/"
      "/.zed/"
    ];
    signing = {
      signByDefault = true;
      key = null;
    };
    extraConfig = {
      init.defaultBranch = "main";
      push.autoSetupRemote = true;
    };
    delta = {
      enable = true;
      options = {
        side-by-side = true;
        line-numbers = true;
      };
    };
  };
}
