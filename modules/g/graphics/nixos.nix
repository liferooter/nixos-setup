{ lib, config, ... }:
{
  config = lib.mkIf config.setup.isLaptop {
    hardware.graphics.enable = true;
  };
}
