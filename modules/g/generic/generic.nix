{ lib, systemConfig, util, ... }:
{
  util.mkGenericOption = { homeDefault ? _: args.default, ... }@args:
    let
      sharedArgs = lib.removeAttrs args [ "homeDefault" ];
    in
    lib.mkOption (sharedArgs
      // (lib.optionalAttrs (systemConfig != null)
      { default = homeDefault systemConfig; }));

  util.mkGenericEnableOption = label: func: util.mkGenericOption {
    description = "Whether to enable ${label}";
    type = lib.types.bool;
    default = false;
    homeDefault = func;
  };
}
