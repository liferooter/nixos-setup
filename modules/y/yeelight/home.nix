{ lib, pkgs, inputs, config, ... }:
let
  cfg = config.programs.yeelight;
in
{
  options = {
    programs.yeelight = {
      enable = lib.mkEnableOption "Yeelight CLI";
      defaultLamp = lib.mkOption {
        description = "Default Yeelight lamp address or name";
        type = with lib.types; nullOr str;
        default = null;
      };
    };
  };
  config = lib.mkIf cfg.enable {
    home.packages = [
      inputs.yeelight.packages.${pkgs.system}.default
    ];

    home.sessionVariables = lib.optionalAttrs (cfg.defaultLamp != null) {
      YEELIGHT_ADDR = "shadow-sun";
    };

    programs.nushell.extraConfig = ''
      use ${./yeelight.nu} *
    '';
  };
}
