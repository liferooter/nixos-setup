# A CLI to control your Yeelight smart lights
export extern "yeelight" [
  --help (-h)    # Prints help information
  --version (-V) # Prints version information

  --port (-p): int
  --timeout (-t): int

  address?: string # The IP address or name of the bulb
]

def adjust-properties [] {
  [ Bright Ct Color ]
}

def adjust-actions [] {
  [ Increase Decrease Circle ]
}

# Adjust properties
export extern "yeelight adjust" [
  --bg           # Perform action on background light
  --help (-h)    # Prints help information
  --version (-V) # Prints version information

  property: string@adjust-properties
  action: string
]

# Adjust properties with percentage
export extern "yeelight adjust-percent" [
  --bg           # Perform action on background light
  --help (-h)    # Prints help information
  --version (-V) # Prints version information

  property: string@adjust-properties
  percent: int
  duration: int = 500
]

# Search for lamps in the network
export extern "yeelight discover" [
  --help (-h)    # Prints help information
  --version (-V) # Prints version information

  --duration: int = 5000
]

def flow-action [] {
  [ Recover Stay Off ]
}

# Start color flow
export extern "yeelight flow" [
  --bg           # Perform action on background light
  --help (-h)    # Prints help information
  --version (-V) # Prints version information

  expression: string
  count: int = 0
  action: string@flow-action = Recover
]

# Stop color flow
export extern "yeelight flow-stop" [
  --bg           # Perform action on background light
  --help (-h)    # Prints help information
  --version (-V) # Prints version information
]

def gettable-properties [] {
  [ Power Bright Ct Rgb Hue Sat
    ColorMode Flowing DelayOff
    FlowParams MusicOn Name BgPower
    BgFlowing BgFlowParams BgCt
    BgColorMode BgBright BgRgb BgHue
    BgSat NightLightBright ActiveMode ]
}

# Get properties
export extern "yeelight get" [
  --json         # Output in JSON format
  --help (-h)    # Prints help information
  --version (-V) # Prints version information

  ...properties: string@gettable-properties
]

# Prints this message or the help of the given subcommand(s)
export extern "yeelight help" [
  ...subcommand: string
]

# Listen to notifications from lamp
export extern "yeelight listen" [
  --help (-h)    # Prints help information
  --version (-V) # Prints version information
]

# Connect to music TCP stream
export extern "yeelight music-connect" [
  --help (-h)    # Prints help information
  --version (-V) # Prints version information

  host: string
  port: int
]

# Stop music mode
export extern "yeelight music-stop" [
  --help (-h)    # Prints help information
  --version (-V) # Prints version information
]

def effects [] {
  [ Sudden Smooth ]
}

def modes [] {
  [ Normal Ct Rgb Hsv Cf NightLight ]
}

# Turn off light
export extern "yeelight off" [
  --bg           # Perform action on background light
  --help (-h)    # Prints help information
  --version (-V) # Prints version information

  --duration (-d): int            = 500
  --effect   (-e): string@effects = Smooth
  --mode     (-m): string@modes   = Normal
]

# Turn on light
export extern "yeelight on" [
  --bg           # Perform action on background light
  --help (-h)    # Prints help information
  --version (-V) # Prints version information

  --duration (-d): int            = 500
  --effect   (-e): string@effects = Smooth
  --mode     (-m): string@modes   = Normal
]

def presets [] {
  [ Candle Reading NightReading CosyHome
    Romantic Birthday DateNight Teatime PcMode
    Concentration Movie Night Notify Notify2 PulseRed
    PulseBlue PulseGreen Red Green Blue Police
    Police2 Disco Temp ]
}

# Presets
export extern "yeelight preset" [
  --help (-h)    # Prints help information
  --version (-V) # Prints version information

  preset: string@presets
]

def settable-properties [] {
  [ bright ct default hsv name power rgb scene ]
}

# Set values
export extern "yeelight set" [
  --help (-h)    # Prints help information
  --version (-V) # Prints version information

  --duration (-d): int            = 500
  --effect   (-e): string@effects = Smooth

  property: string@settable-properties
  value: string
]

# Start timer
export extern "yeelight timer" [
  --help (-h)    # Prints help information
  --version (-V) # Prints version information

  minutes: int
]

# Clear current timer
export extern "yeelight timer-clear" [
  --help (-h)    # Prints help information
  --version (-V) # Prints version information
]

# Get remaining minutes for timer
export extern "yeelight timer-get" [
  --help (-h)    # Prints help information
  --version (-V) # Prints version information
]

# Toggle light
export extern "yeelight toggle" [
  --bg           # Perform action on background light
  --dev          # Perform action on all lights of device
  --help (-h)    # Prints help information
  --version (-V) # Prints version information
]
