{ lib, pkgs, config, ... }:
{
  options = {
    programs.trash-cli.enable = lib.mkEnableOption "trash directory CLI" // {
      default = config.setup.isLaptop && config.home.username != "root";
    };
  };

  config = lib.mkIf config.programs.trash-cli.enable {
    home.packages = [ pkgs.trash-cli ];
    home.shellAliases = {
      rm = lib.getExe' pkgs.rmtrash "rmtrash";
      rmdir = lib.getExe' pkgs.rmtrash "rmdirtrash";
    };
  };
}
