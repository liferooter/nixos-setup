{ ... }:
{
  config.programs.tmux = {
    mouse = true;
    prefix = "`";
    terminal = "xterm-256color";
    extraConfig = ''
      set -g status off
    '';
  };
}
