{ lib, pkgs, config, ... }:
{
  options = {
    programs.nix-search.enable = lib.mkEnableOption "nix-search utility";
  };
  config = lib.mkMerge [
    (lib.mkDefault {
      programs.nix-search.enable = config.setup.isLaptop;
    })
    (lib.mkIf config.programs.nix-search.enable {
      home.packages = [ pkgs.nix-search ];
    })
    (lib.mkDefault {
      programs.nix-search.enable = config.setup.isLaptop;
    })
  ];
}
