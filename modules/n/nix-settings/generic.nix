{ lib
, self
, pkgs
, systemConfig
, mode
, config
, ...
}:
let
  lockedInput = input:
    let
      lockFile = lib.importJSON "${self}/flake.lock";
      lockNode = lockFile.nodes.${input} or (abort "Input `${input}` is not found");
    in
    lockNode.locked;
in
{
  config = lib.mkIf (systemConfig == null) (lib.mkMerge [
    {
      nixpkgs.config.allowUnfree = true;
      nix = {
        package = pkgs.nix;
        gc = {
          automatic = true;
          options = "--delete-older-than 7d";
        };
        settings = {
          experimental-features = [
            "nix-command"
            "flakes"
          ];
          trusted-users = [
            "liferooter"
          ];
        };
        registry = {
          nixpkgs.to = lockedInput "nixpkgs";
          templates.to = {
            type = "gitlab";
            owner = "liferooter";
            repo = "nix-templates";
          };
        };
      };
    }
    (lib.optionalAttrs (mode == "nixos") {
      nix.channel.enable = false;
      nix.gc.dates = "weekly";
      nix.optimise = {
        automatic = true;
        dates = [ "dayly" ];
      };
      programs.nix-ld.enable = true;
    })
    (lib.optionalAttrs (mode == "home") {
      nix.gc.frequency = "weekly";
    })
    (lib.optionalAttrs (mode == "home" && systemConfig == null) {
      home.packages = [
        config.nix.package
      ];
    })
  ]);
}
