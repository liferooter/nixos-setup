{ pkgs
, self
, lib
, config
, ...
}:
{
  options.programs.nethack.enable = lib.mkEnableOption "Whether to enable NetHack game";
  config = lib.mkIf config.programs.nethack.enable {
    home.packages = [ self.packages.${pkgs.system}.nethack ];
    home.file.".nethackrc".text =
      let
        options = {
          fruit = "pineapple";

          tutorial = false;

          autopickup = false;
          implicit_uncursed = false;

          showexp = true;
          hitpointbar = true;

          time = true;
          timed_delay = true;
          runmode = "run";

          statushilite = 1;

          windowtype = "tty";
          altmeta = true;
          popup_dialog = true;
          splash_screen = true;
          perminv_mode = "all";
          windowborders = 0;
          statuslines = 3;
          color = true;
          menucolors = true;

          symset = "Enhanced1";
          boulder = "0";
          lit_corridor = true;
          hilite_pet = true;
          hilite_pile = true;
          showrace = true;
        };

        hiliteStatus = {
          gold.always = "yellow";
          armor-class = {
            down = "blue";
            up = "yellow";
          };
          alignment = {
            Lawful = "lightblue";
            Neutral = "green";
            Chaotic = "magenta";
          };
          hunger.always = "yellow";
          characteristics = {
            up = "green";
            down = "red";
          };

          hitpoints = {
            "100%" = "brightgreen";
            "<100%" = "green";
            "<66%" = "yellow";
            "<50%" = "orange";
            "<33%" = [
              "red"
              "bold"
            ];
            "<15%" = [
              "red"
              "inverse"
            ];
          };

          power = {
            "100%" = "white";
            "<100%" = "lightblue";
            "<66%" = "brightmagenta";
            "<50%" = "blue";
            "<33%" = "yellow";
            "<15%" = "orange";
            "<5" = "black";
          };

          condition = {
            major = [
              "orange"
              "inverse"
            ];
            "lev+fly" = [
              "blue"
              "inverse"
            ];
          };
        };

        binds =
          (lib.concatMapAttrs
            (key: direction: {
              ${key} = "move${direction}";
              ${lib.toUpper key} = "run${direction}";
              "^${key}" = "rush${direction}";
            })
            {
              n = "west";
              e = "south";
              i = "north";
              o = "east";
              l = "northwest";
              u = "northeast";
              k = "southwest";
              m = "southeast";
            }
          )
          // {
            M-o = "open";
            M-O = "offer";
            M-i = "inventory";
            M-I = "inventtype";
            "M-^i" = "invoke";
            M-e = "eat";
            M-E = "engrave";
            h = "enhance";
            y = "reqmenu";
            B = "chronicle";
            b = "overview";
            M-q = "quit";
            C-c = "quit";
          };
        menuColors = {
          "named B($| )" = "lightblue";
          "named C($| )" = "orange";
          "named U($| )" = "green";
          "named N($| )" = "brightmagenta";
          " blessed " = [
            "lightblue"
            "bold"
          ];
          " cursed " = [
            "orange"
            "bold"
          ];
          " uncursed " = [
            "green"
            "bold"
          ];
          " holy " = "cyan";
          " unholy " = "red";

          "gold piece" = "yellow";
          "\\(unpaid," = "black";

          "(gone)" = "black";

          "\\[Unskilled\\]" = "orange";
          "\\[Basic\\]" = "yellow";
          "\\[Skilled\\]" = [
            "blue"
            "bold"
          ];
          "\\[Expert\\]" = [
            "lightgreen"
            "bold"
          ];
          "\\[Master\\]" = [
            "lightmagenta"
            "bold"
          ];
        };

        colorToString =
          color: if builtins.typeOf color == "list" then lib.concatStringsSep "&" color else color;

        inherit (lib) concatLines pipe mapAttrsToList
          concatStringsSep flatten flip;
      in
      concatLines (
        (pipe options [
          (mapAttrsToList (
            option: value:
              if value == true then
                option
              else if value == false then
                "!${option}"
              else
                "${option}:${toString value}"
          ))
          (map (option: "OPTION=${option}"))
        ])
        ++ (pipe hiliteStatus [
          (mapAttrsToList (
            field: states:
              concatStringsSep "/" (flatten [
                field
                (mapAttrsToList
                  (state: color: [
                    state
                    (colorToString color)
                  ])
                  states)
              ])
          ))
          (map (hilite: "OPTIONS=hilite_status:${hilite}"))
        ])
        ++ (flip mapAttrsToList binds (key: command: "BIND=${key}:${command}"))
        ++ (flip mapAttrsToList menuColors (
          pattern: color: "MENUCOLOR=\"${pattern}\"=${colorToString color}"
        ))
      );
  };
}
