load-env {
  config: ($env.config? | default {} | merge {
    color_config: (catppuccin-mocha)
    completions: {
      external: {
        max_results: 5000
      }
    }
    cursor_shape: {
      emacs: line
      vi_insert: line
      vi_normal: block
    }
    highlight_resolved_externals: true
    history: {
      file_format: sqlite
      isolation: true
    }
    show_banner: false
    table: {
      mode: light
    }
    rm: {
      always_trash: true
    }
    hooks: {
      pre_execution: { print -n "\u{1b}[0 q" }
    }
  })
}

def __flakes [ context:string ] {
  nix registry list
  | lines
  | parse '{type} flake:{name} {url}'
  | get name
}

# Enter some flake's directory
def --env cd-flake [
  flake: string@__flakes # Flake to enter
] {
  nix flake metadata $flake --json
    | from json
    | get path
    | cd $in
}
