# Load environment variables from shell script
def --env load-sh-env [
  script: path # Shell script to load environment from
] {
  [
    (bash -c 'env -0')
    (bash -c $'
      source ($script)
      env -u SHLVL -0
    ')
  ]
  | each {|vars|
    $vars
    | split row "\u{0}"
    | parse '{key}={value}'
  }
  | reduce {|after, before|
    $after
    | where {|line| $line not-in $before}
    | transpose --header-row --as-record
  }
  | into record
  | load-env
}

# Prints my "Name <e@mail>"
def me []: nothing -> string {
  $"(git config user.name) <(git config user.email)>"
}
