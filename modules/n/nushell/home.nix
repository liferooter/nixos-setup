{ lib
, pkgs
, config
, systemConfig
, mode
, inputs
, ...
}:
{
  config.programs.nushell = {
    enable = lib.mkDefault (systemConfig != null
      && systemConfig.users.users.${config.home.username}.shell.pname == "nushell");
    extraEnv = ''
      use ${./env.nu}
      source ${./utils.nu}

      load-sh-env ${config.home.sessionVariablesPackage}/etc/profile.d/hm-session-vars.sh
    '' + lib.optionalString (systemConfig == null && mode == "home") ''
      load-sh-env /nix/var/nix/profiles/default/etc/profile.d/nix.sh
    '';
    extraConfig = ''
      use ${inputs.nu-scripts}/themes/nu-themes/catppuccin-mocha.nu
      source ${./config.nu}

      def run-and-notify [ message:string cmd:closure ] {
        try { do $cmd }
        ${lib.getExe' pkgs.libnotify "notify-send"} -a Nushell $message
      }
    '' + lib.optionalString config.programs.starship.enable ''
      $env.TRANSIENT_PROMPT_COMMAND = {|| starship module character}
    '';
  };
}
