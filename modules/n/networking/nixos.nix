{ lib, config, hostname, ... }:
{
  options = {
    setup.server.network = with lib; {
      interface = mkOption {
        description = "Default network interface name";
        type = with types; str;
        default = "ens3";
      };
      address = mkOption {
        description = "IPv4 address of the server";
        type = with types; str;
      };
      gateway = mkOption {
        description = "Gateway IPv4 address";
        type = with types; str;
        default = "10.0.0.1";
      };
    };
  };
  config = lib.mkMerge [
    {
      networking.hostName = hostname;
      networking.nameservers = lib.mkDefault [ "1.1.1.1" ];
    }
    (lib.mkIf config.setup.isLaptop {
      services.avahi.enable = true;
      networking = {
        networkmanager.enable = true;
        firewall.enable = false;
      };
    })
    (lib.mkIf config.setup.isServer {
      systemd.network = {
        enable = true;

        networks.main =
          let
            cfg = config.setup.server.network;
          in
          {
            matchConfig.Name = cfg.interface;
            address = [ "${cfg.address}/32" ];
            dns = config.networking.nameservers;
            routes = [{
              Gateway = cfg.gateway;
              GatewayOnLink = true;
            }];
          };
      };
      networking.useDHCP = false;
    })
  ];
}
