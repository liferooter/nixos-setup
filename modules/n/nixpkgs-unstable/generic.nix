{ inputs, lib, pkgs, ... }:
{
  options = {
    test.config = lib.mkOption {
      description = "";
      type = lib.types.anything;
      default = pkgs.config;
    };
  };
  config = {
    _module.args.pkgs-unstable = import inputs.nixpkgs-unstable
      {
        system = pkgs.system;
        config = pkgs.config;
      };
  };
}
