{ lib, config, modulesPath, ... }:
{
  imports = [
    ({ config, lib, ... }@args:
      lib.mkIf config.hardware.isVM
        (import (modulesPath + "/profiles/qemu-guest.nix") args))
  ];
  options = {
    hardware.isVM = lib.mkOption {
      description = "Whether the server is a virtual machine";
      type = with lib.types; bool;
      default = config.setup.isServer;
    };
  };
  config = lib.mkIf config.hardware.isVM
    {
      services.qemuGuest.enable = true;
      boot.initrd.availableKernelModules = [
        "uhci_hcd"
        "virtio_pci"
        "sr_mod"
        "virtio_blk"
      ];
    };
}
