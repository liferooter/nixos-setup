{ pkgs, lib, config, ... }:
{
  options = {
    home.useDefaultPackages = lib.mkOption {
      description = "Whether to use default package set.";
      default = true;
      type = lib.types.bool;
    };
  };
  config = lib.mkIf config.home.useDefaultPackages {
    home.packages = [ pkgs.ripgrep pkgs.fd ]
      ++ [ pkgs.zip pkgs.unzip ]
      ++ [ pkgs.htop pkgs.aria2 ]
      ++ [ pkgs.file pkgs.tree ]
      ++ [ pkgs.wget ]
      ++ lib.optionals config.setup.isLaptop [
      pkgs.wl-clipboard
      pkgs.glow
    ];

    programs = {
      bat.enable = true;

      direnv = {
        enable = true;
        nix-direnv.enable = true;
      };
      zoxide.enable = true;

      tealdeer.enable = true;
      carapace.enable = lib.mkDefault config.programs.nushell.enable;
    };
  };
}
