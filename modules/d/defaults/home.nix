{ config, ... }:
{
  config.programs.man.generateCaches = config.setup.isLaptop;
}
