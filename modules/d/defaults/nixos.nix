{ lib, config, ... }:
let
  inherit (lib) mkDefault;
in
{
  config = lib.mkMerge [
    (mkDefault {
      boot.tmp.cleanOnBoot = true;
      boot.loader.timeout = 0;
      documentation = {
        doc.enable = false;
        man.enable = true;
        # FIXME: enabling this option breaks option extending.
        # nixos.includeAllModules = true;
      };
      zramSwap = {
        enable = true;
        memoryPercent = 200;
      };
      services.resolved.enable = true;
    })
    {
      documentation.man.generateCaches = config.setup.isLaptop;
    }
  ];
}
