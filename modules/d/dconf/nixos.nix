{ lib, config, ... }:
{
  programs.dconf.enable = lib.mkDefault config.setup.isLaptop;
}
