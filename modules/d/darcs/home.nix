{ lib, config, ... }:
lib.mkIf config.setup.isLaptop {
  programs.darcs = {
    enable = lib.mkDefault true;
    author = ["Gleb Smirnov <glebsmirnov0708@gmail.com>"];
    boring = [
      "^.direnv(/|$)"
    ];
  };
}
