{ pkgs, lib, config, ... }:
let
  cfg = config.setup.server.ssh;
in
{
  options.setup.server.ssh = {
    publicKey = lib.mkOption {
      description = "SSH public key for accessing the server";
      type = with lib.types; str;
      default = null;
    };
  };
  config = lib.mkIf config.setup.isServer {
    setup.server.ssh.publicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPqqape1/IJC8PK+7lJxwM9N9Oo4SK7HZ7SnCMZjmaTR liferooter@computer";

    environment.systemPackages = [
      pkgs.ncurses
    ];
    services.openssh = {
      enable = true;
      settings = {
        PasswordAuthentication = false;
        AcceptEnv = "COLORTERM";
      };
    };
    users.users.liferooter.openssh.authorizedKeys.keys = [
      cfg.publicKey
    ];
  };
}
