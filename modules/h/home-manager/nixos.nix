{ self, hostname, config, lib, options, inputs, ... }:
let
  homesDir = "${self}/homes";
  homes = lib.pipe homesDir [
    builtins.readDir
    (lib.concatMapAttrs (dir: _:
      let
        components = lib.splitString "@" dir;
      in
      if lib.length components == 1
      then { } # It's a standalone configuration
      else if lib.length components == 2
      then if lib.elemAt components 1 == hostname
      then { ${lib.elemAt components 0} = "${homesDir}/${dir}"; }
      else { } # Different host
      else abort "Invalid home name: ${dir}"
    ))
  ];
in
{
  imports = [
    inputs.home-manager.nixosModules.home-manager
  ];
  config = lib.optionalAttrs (lib.hasAttr "home-manager" options) {
    home-manager = {
      useGlobalPkgs = true;
      useUserPackages = true;
      extraSpecialArgs = {
        inherit self inputs hostname;
        systemConfig = config;
        mode = "home";
      };
      sharedModules = [
        "${self}/modules.nix"
      ];

      users = lib.mkMerge [
        homes
        (lib.genAttrs [ "root" "liferooter" ] (username: { }))
      ];
    };

    users.users.liferooter = {
      isNormalUser = true;
      extraGroups = [
        "wheel"
      ] ++ lib.optionals config.setup.isLaptop [
        "networkmanager"
        "video"
        "scanner"
        "lp"
      ];
    };
  };
}
