{ pkgs-unstable
, lib
, inputs
, ...
}:
let
  inherit (lib) getExe;
in
{
  imports = [
    ./typst
    ./nix
    ./settings.nix
  ];

  programs.helix = {
    enable = lib.mkDefault true;
    package = pkgs-unstable.helix;
    defaultEditor = lib.mkDefault true;

    languages = {
      language-server = {
        tinymist.command = "tinymist"; # Search for tinymist in the environment
        nixd.command = getExe pkgs-unstable.nixd;
        rust-analyzer.config.rust-analyzer = {
          check.command = "clippy";
        };
        metals.config.metals = {
          autoImportBuild = "all";
        };
      };

      use-grammars.except = [ "gemini" ];
    };
    themes.custom = {
      inherits = "catppuccin_mocha";
      "ui.bufferline.active" = { fg = "mauve"; bg = "base"; };
      "ui.background" = { fg = "text"; };
    };
  };
  xdg.configFile."helix/themes" = {
    source = "${inputs.catppuccin-helix}/themes/default";
    recursive = true;
  };
}
