{ pkgs-unstable, lib, ... }:
{
  config.programs.helix.languages.language = [
    {
      name = "typst";
      scope = "source.typst";
      file-types = [
        "typst"
        "typ"
      ];
      indent = {
        tab-width = 2;
        unit = "  ";
      };
      comment-token = "//";
      injection-regex = "typ(st)?";
      language-servers = [ "tinymist" ];
      formatter.command = lib.getExe pkgs-unstable.typstyle;
      roots = [ "template.typ" ];

      auto-pairs = {
        "(" = ")";
        "{" = "}";
        "[" = "]";
        "$" = "$";
        "\"" = "\"";
      };
    }
  ];
}
