{ inputs, lib, config, ... }:
{
  config = lib.mkIf config.programs.helix.enable {
    xdg.configFile."helix/runtime/queries/typst".source = "${inputs.tree-sitter-typst-src}/queries/typst";
    xdg.configFile."helix/runtime/sources/grammars/typst".source = inputs.tree-sitter-typst-src;
  };
}
