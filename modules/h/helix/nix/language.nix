{ lib, pkgs, ... }:
{
  config.programs.helix.languages.language = [
    {
      name = "nix";
      formatter.command = lib.getExe pkgs.nixpkgs-fmt;
      language-servers = [ "nixd" ];
    }
  ];
}
