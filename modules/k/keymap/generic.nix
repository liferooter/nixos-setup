{ util, config, mode, lib, ... }:
{
  options = {
    setup.keymap = {
      colemak.enable = util.mkGenericEnableOption "Colemak keyboard layout"
        (cfg: cfg.setup.keymap.colemak.enable);
      useDefaultTweaks = util.mkGenericEnableOption "default keymap tweaks"
        (cfg: cfg.setup.keymap.useDefaultTweaks);
    };
  };
  config =
    let
      cfg = config.setup.keymap;
      xkbOpts = lib.mkMerge [
        {
          layout = "us,ru";
          variant = lib.mkDefault ",";
          options = (if mode == "nixos"
            then lib.id
            else lib.singleton) "grp:win_space_toggle";
        }
        (lib.mkIf cfg.colemak.enable {
          variant = "colemak,";
        })
        (lib.mkIf cfg.useDefaultTweaks {
          options = (if mode == "nixos"
          then lib.concatStringsSep ","
          else lib.id) [
            "compose:ins"
            "ctrl:swap_lalt_lctl"
            "lv3:ralt_alt"
          ];
        })
      ];
    in
    if mode == "nixos" then {
      services.xserver.xkb = xkbOpts;
    } else {
      home.keyboard = xkbOpts;
    };
}
