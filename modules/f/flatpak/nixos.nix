{ lib, config, ... }:
{
  config = {
    services.flatpak.enable = lib.mkDefault config.setup.isLaptop;
    xdg.portal = {
      enable = lib.mkDefault config.setup.isLaptop;
      xdgOpenUsePortal = true;
    };
  };
}
