{ lib, config, ... }:
{
  programs.fish.enable =
    let
      isFish = pkg: pkg.pname == "fish";
    in
    lib.mkDefault (
      isFish config.users.defaultUserShell
      || lib.any (user: isFish user.shell) (lib.attrValues config.users.users)
    );
}
