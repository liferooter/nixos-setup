{ lib, config, systemConfig, pkgs, ... }:
let
  inherit (lib) getExe;
in
{
  programs.fish = {
    enable = lib.mkDefault (systemConfig != null
      && systemConfig.users.users.${config.home.username}.shell.pname == "fish");
    shellAbbrs = {
      g = "git";
    };
    interactiveShellInit = ''
      function cd-flake -d "Enter flake source directory" -a 'flake'
        if [ (count $argv) != 1 ]
          echo "`cd-flake` requires exactly one argument"
          return 1
        end

        set -l path (
          ${getExe pkgs.nix} flake metadata $argv[1] --json | ${getExe pkgs.jq} -r .path
        )

        cd $path
      end

      function __flakes_in_registry
        nix registry list \
          | cut -d' ' -f2 \
          | cut -d: -f2
      end

      complete -c cd-flake -a "(__flakes_in_registry)"
    '';
  };
  programs.starship.enableTransience = lib.mkDefault config.programs.fish.enable;
  programs.eza = {
    enable = lib.mkDefault config.programs.fish.enable;
    git = true;
  };
}
