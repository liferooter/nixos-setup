{ pkgs
, lib
, util
, config
, mode
, ...
}:
{
  options.fonts =
    let
      inherit (util) mkGenericOption;
      inherit (lib) mkOption types;
    in
    {
      googleFonts = mkGenericOption {
        type = with types; listOf str;
        description = "List of fonts to install from Google Fonts";
        default = [ ];
      };
      nerdFonts = mkOption {
        type = with types; listOf str;
        default = [ ];
        description = "List of fonts to install from Nerd Fonts";
      };

      preferred = {
        monospace = mkGenericOption {
          type = with types; str;
          default = "Monospace";
          homeDefault = cfg: cfg.fonts.preferred.monospace;
          description = "Preferred monospace font";
        };
        sansSerif = mkGenericOption {
          type = with types; str;
          default = "Noto Sans";
          homeDefault = cfg: cfg.fonts.preferred.sansSerif;
          description = "Preferred sans-serif font";
        };
        emoji = mkGenericOption {
          type = with types; str;
          default = "Noto Color Emoji";
          homeDefault = cfg: cfg.fonts.preferred.emoji;
          description = "Preferred Emoji font";
        };
        useFontconfig = mkGenericOption {
          type = with types; bool;
          default = false;
          homeDefault = cfg: cfg.fonts.preferred.useFontconfig;
          description = "Whether to use preferred fonts as Fontconfig defaults";
        };
      };
    };

  config =
    let
      cfg = config.fonts;
      fontPackages = lib.optional (cfg.googleFonts != [ ])
        (pkgs.google-fonts.override {
          fonts = cfg.googleFonts;
        })
      ++ lib.optional (cfg.nerdFonts != [ ]) (pkgs.nerdfonts.override {
        fonts = cfg.nerdFonts;
      })
      ++ (with pkgs;
        [
          noto-fonts
          noto-fonts-cjk-sans
          noto-fonts-emoji
        ]);
    in
    lib.mkMerge [
      (if (mode == "nixos")
      then {
        fonts.packages = fontPackages;
      } else {
        home.packages = fontPackages;
      })
      ({
        fonts.fontconfig.defaultFonts = lib.optionalAttrs cfg.preferred.useFontconfig {
          monospace = lib.optional (cfg.preferred.monospace != "Monospace") cfg.preferred.monospace;
          sansSerif = [ cfg.preferred.sansSerif ];
          emoji = [ cfg.preferred.emoji ];
        };

        sass.includePath = [
          (pkgs.writeTextFile {
            name = "preferred-fonts-sass";
            text =
              let
                fonts = config.fonts.preferred;
              in
              ''
                $monospace: ${fonts.monospace};
                $sansSerif: ${fonts.sansSerif};
              '';
            destination = "/_fonts.scss";
          })
        ];
      })
    ];
}
