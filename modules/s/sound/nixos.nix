{ lib, config, ... }:
{
  config = lib.mkIf config.setup.isLaptop {
    services.pipewire = {
      enable = true;
      alsa.enable = true;
      pulse.enable = true;
      jack.enable = true;
    };
    security.rtkit.enable = true;
    security.pam.loginLimits = [
      {
        domain = "@wheel";
        item = "memlock";
        type = "-";
        value = "unlimited";
      }
      {
        domain = "@wheel";
        item = "rtprio";
        type = "-";
        value = "95";
      }
    ];
  };
}
