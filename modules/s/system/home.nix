{ lib, config, systemConfig, ... }:
{
  config = lib.optionalAttrs (systemConfig == null) {
    nixpkgs.system = config.setup.system;
  };
}
