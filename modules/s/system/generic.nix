{ lib, systemConfig, mode, ... }:
{
  options = {
    setup.system = lib.mkOption {
      description = "System to use the generic option";
      type = lib.types.str;
    } // lib.optionalAttrs (systemConfig != null) {
      default = systemConfig.setup.system;
      readOnly = true;
    } // lib.optionalAttrs (systemConfig == null && mode == "home") {
      default = null;
    };
  };
}
