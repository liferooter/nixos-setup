{ pkgs, lib, ... }:
{
  config.programs.starship = {
    enable = lib.mkDefault true;
    settings = lib.mergeAttrsList [
      (lib.importTOML "${pkgs.starship}/share/starship/presets/no-runtime-versions.toml")
      {
        nix_shell = {
          format = "via [$symbol]($style) ";
          heuristic = true;
        };
      }
    ];
  };
}
