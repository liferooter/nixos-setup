{ pkgs, lib, config, ... }:
let
  inherit (config.setup) isLaptop;
in
{
  programs.gpg.enable = lib.mkDefault true;
  programs.ssh = {
    enable = lib.mkDefault true;
    addKeysToAgent = "yes";
    matchBlocks."*".setEnv = {
      COLORTERM = "truecolor";
      TERM = "xterm-256color";
    };
    extraConfig = "Include config.d/*";
  };
  services.gpg-agent = {
    enable = lib.mkDefault config.programs.gpg.enable;
    pinentryPackage = lib.mkDefault (
      if isLaptop
      then pkgs.pinentry-gnome3 else pkgs.pinentry-curses
    );
  };
  services.ssh-agent = {
    enable = lib.mkDefault config.programs.ssh.enable;
  };
}
