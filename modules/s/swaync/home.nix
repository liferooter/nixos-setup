{ lib, pkgs, config, inputs, util, ... }:
let
  catppuccinSwaync = util.compileSass {
    file = pkgs.writeText "catppuccin-swaync.scss" ''
      @import "mocha";
      @import "theme";
    '';
    runBefore = ''
      cp -r ${inputs.catppuccin-swaync}/src/_theme.scss .
    '';
    includePath = [
      "."
    ];
  };
in
{
  config = lib.mkIf (config.catppuccin.enable && config.services.swaync.enable) {
    services.swaync.style = builtins.readFile catppuccinSwaync;
    systemd.user.services.swaync.Unit.After = [ "graphical-session.target" ];
  };
}
