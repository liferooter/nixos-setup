{ self, config, inputs, hostname, ... }:
{
  imports = [
    inputs.sops-nix.homeManagerModules.sops
  ];
  sops = {
    defaultSopsFile = "${self}/secrets/${hostname}/${config.home.username}.yml";
    age.sshKeyPaths = [ "${config.home.homeDirectory}/.config/sops/age/key.txt" ];
    age.generateKey = true;
  };
}
