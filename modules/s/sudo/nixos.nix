{ lib, config, ... }:
{
  security.sudo.wheelNeedsPassword = lib.mkDefault config.setup.isLaptop;
}
