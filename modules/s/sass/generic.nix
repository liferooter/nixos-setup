{ lib, util, pkgs, config, ... }:
{
  options = {
    sass.includePath = util.mkGenericOption {
      description = "Paths to add to include path for Sass files compiled via `util.compileSass`";
      type = with lib.types; listOf path;
      default = [ ];
      homeDefault = cfg: cfg.sass.includePath;
    };
  };
  config = {
    util.compileSass =
      { file
      , name ? "style.css"
      , includePath ? [ ]
      , extraAttrs ? { }
      , runBefore ? ""
      , runAfter ? ""
      }:
      pkgs.runCommand name extraAttrs ''
        ${runBefore}
        ${lib.getExe pkgs.sassc} ${file} ${
          lib.concatMapStringsSep " " (path: "-I ${path}") (includePath ++ config.sass.includePath)
        } $out
        ${runAfter}
      '';
  };
}
