{ pkgs, lib, config, ... }:
{
  config = lib.mkMerge [
    (lib.mkIf config.documentation.man.enable {
      environment.systemPackages = [
        pkgs.man-pages
        pkgs.man-pages-posix
      ];
    })
    (lib.mkIf config.setup.isLaptop {
      documentation.dev.enable = true;
    })
  ];
}
