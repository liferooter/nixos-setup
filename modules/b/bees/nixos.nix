{ config, lib, ... }:
{
  options = {
    services.beesd.enableForRoot = lib.mkEnableOption "deduplication of BTRFS root";
  };
  config =
    let
      beesEnabled = config.services.beesd.enableForRoot;
      rootIsBtrfs = config.fileSystems."/".fsType == "btrfs";

    in
    lib.mkIf
      (beesEnabled && lib.assertMsg (beesEnabled -> rootIsBtrfs)
        "bees must not be enabled on systems without BTRFS root")
      {
        services.beesd.filesystems.root = {
          spec = "/";
          hashTableSizeMB = lib.mkDefault 4096;
          extraOptions = lib.mkDefault [ "--thread-count" "4" ];
        };
      };
}
