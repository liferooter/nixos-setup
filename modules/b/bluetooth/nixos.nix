{ lib, config, ... }:
{
  config = lib.mkIf config.setup.isLaptop {
    hardware.bluetooth.enable = true;
  };
}
