{ pkgs, config, lib, ... }:
{
  config = lib.mkIf config.setup.isLaptop {
    services.printing = {
      enable = lib.mkDefault true;
      drivers = with pkgs; [
        hplipWithPlugin # For HP printers
        cnijfilter2 #     For Canon printers
      ];
    };
    hardware.sane = {
      enable = lib.mkDefault true;
      extraBackends = [
        pkgs.hplipWithPlugin pkgs.sane-airscan
      ];
    };
    services.ipp-usb.enable = true;
  };
}
