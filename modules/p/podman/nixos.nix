{ lib, config, ... }:
let
  podmanPkg = config.virtualisation.podman.package;
  serviceName = name: "podman-network-${name}";
  mkService = name: options:
    let
      optionToArgs = key: value:
        let
          keyArg = "--${key}";
        in
        if lib.isBool value
        then lib.optional value keyArg
        else if lib.isList value
        then lib.concatMap (value: [ keyArg value ]) value
        else [ keyArg value ];
      podmanArgs = lib.flatten (lib.mapAttrsToList optionToArgs options);
      podman = lib.getExe podmanPkg;
      removeNetwork = "${podman} network rm -f ${name}";
    in
    {
      ${serviceName name} = {
        wantedBy = [ "multi-user.target" ];
        serviceConfig = {
          Type = "oneshot";
          RemainAfterExit = true;
          ExecStartPre = "-${removeNetwork}";
          ExecStart = "${podman} network create ${lib.escapeShellArgs podmanArgs} ${name}";
          ExecStop = removeNetwork;
        };
      };
    };
in
{
  options =
    {
      virtualisation.oci-containers = {
        networks = lib.mkOption {
          description = "Podman networks to initialize for containers";
          type = with lib.types; attrsOf (attrsOf (oneOf [ bool str (listOf str) ]));
          default = { };
        };
        containers = lib.mkOption {
          type = lib.types.attrsOf (lib.types.submodule ({ config, ... }: {
            options = {
              restart = lib.mkOption {
                description = "Restart strategy for systemd service";
                type = lib.types.enum [
                  "no"
                  "on-success"
                  "on-failure"
                  "on-abnormal"
                  "on-watchdog"
                  "on-abort"
                  "always"
                ];
                default = "always";
              };
              network = lib.mkOption {
                description = "Podman network to use for the container";
                type = with lib.types; nullOr str;
                default = null;
              };
              after = lib.mkOption {
                description = "Extra units to add to `After` section of systemd unit";
                type = with lib.types; listOf str;
                default = [ ];
              };
            };
            config = {
              extraOptions = lib.optional (config.network != null)
                "--network=${config.network}";
              after = lib.optional (config.network != null)
                "${serviceName config.network}.service";
            };
          }));
        };
      };
    };
  config =
    let
      cfg = config.virtualisation.oci-containers;
    in
    lib.mkMerge [
      {
        # Ensure that virtualisation backend is set to Podman.
        virtualisation.oci-containers.backend = "podman";

        # Fix DNS inside Podman networks
        networking.firewall.interfaces."podman+".allowedUDPPorts = [ 53 5353 ];

        systemd.services = lib.concatMapAttrs mkService cfg.networks;
      }
      {
        systemd.services = lib.concatMapAttrs
          (name: opts: {
            ${opts.serviceName} = {
              after = opts.after;
              serviceConfig.Restart = lib.mkForce opts.restart;
            };
          })
          cfg.containers;
      }
    ];
}
