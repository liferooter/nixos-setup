{ lib, config, ... }:
let
  inherit (lib) mkOption types;
in
{
  options = {
    portServices = mkOption {
      description = "Names of services that requires port allocation";
      type = with types; listOf str;
      default = [ ];
    };
    ports = mkOption {
      description = "Ports allocated for services defined in `portServices`";
      type = types.attrsOf (types.submodule {
        options = {
          str = mkOption {
            description = "Port string";
            type = types.str;
          };
          int = mkOption {
            description = "Port number";
            type = types.int;
          };
        };
      });
      readOnly = true;
    };
  };
  config = {
    ports =
      let
        nPorts = lib.length config.portServices;
      in
      lib.listToAttrs (lib.zipListsWith
        (name: i: {
          inherit name;
          value =
            let
              port = 8000 + i;
            in
            {
              str = toString port;
              int = port;
            };
        })
        config.portServices
        (lib.range 1 nPorts));
  };
}
