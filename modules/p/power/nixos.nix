{ lib, config, ... }:
{
  config = lib.mkIf config.setup.isLaptop {
    services = {
      upower.enable = true;
      power-profiles-daemon.enable = true;
    };
    programs.gamemode.enable = true;
  };
}
