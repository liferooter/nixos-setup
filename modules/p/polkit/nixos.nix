{ lib, config, ... }:
{
  options = with lib; {
    systemd.serviceOwners = mkOption {
      description = "Mapping between system services and users that can manage them";
      type = with types; attrsOf str;
      default = { };
    };
  };
  config.security.polkit = {
    enable = lib.mkDefault true;
    extraConfig =
      let
        hasServiceOwners = config.systemd.serviceOwners != { };
      in
      lib.optionalString
        (hasServiceOwners && lib.assertMsg (hasServiceOwners -> config.security.polkit.enable)
          "Service owners don't work without Polkit") ''
        polkit.addRule(function(action, subject) {
            if (action.id == "org.freedesktop.systemd1.manage-units") {
                var verb = action.lookup("verb");
                if (verb == "start" || verb == "stop" || verb == "restart") {
                    var service = action.lookup("unit").replace(/\.service$/, "");
                    var serviceOwners = ${builtins.toJSON config.systemd.serviceOwners};
                    if (subject.user == serviceOwners[service]) {
                        return polkit.Result.YES;
                    }
                }
            }
        });
      '';
  };
}
