{ lib, config, inputs, ... }:
{
  config = lib.mkMerge [
    {
      programs.alacritty.settings = {
        window = {
          dynamic_padding = lib.mkDefault true;
          padding = {
            x = lib.mkDefault 6;
            y = lib.mkDefault 6;
          };
          opacity = lib.mkDefault 1.0;
        };
        font = {
          normal.family = config.fonts.preferred.monospace;
          size = lib.mkDefault 13;
        };
        mouse.hide_when_typing = lib.mkDefault true;
        keyboard.bindings = [
          {
            mods = "Super | Shift";
            key = "t";
            action = "CreateNewWindow";
          }
        ];
      };
    }
    (lib.mkIf config.catppuccin.enable {
      programs.alacritty.settings.general.import = [
        "${inputs.catppuccin-alacritty}/catppuccin-mocha.toml"
      ];
    })
  ];
}
