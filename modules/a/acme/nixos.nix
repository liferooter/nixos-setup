{ lib, config, ... }:
{
  config = lib.mkIf config.setup.isServer {
    security.acme = {
      acceptTerms = true;
      defaults.email = "glebsmirnov0708@gmail.com";
    };
  };
}
