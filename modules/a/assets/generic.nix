{ lib
, config
, ...
}:
{
  options = with lib; {
    assets = mkOption {
      description = "Attributes passed via `assets` argument.";
      type = with types; attrsOf anything;
      default = { };
    };
  };
  config = {
    _module.args.assets = config.assets;
  };
}
