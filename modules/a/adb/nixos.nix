{ config, lib, ... }:
{
  config = lib.mkIf config.programs.adb.enable {
    users.users.liferooter.extraGroups = [ "adbusers" ];
  };
}
