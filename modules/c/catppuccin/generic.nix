{ inputs
, lib
, util
, pkgs
, config
, ...
}: {
  options.catppuccin.enable = util.mkGenericEnableOption "Catppuccin pallete"
    (cfg: cfg.catppuccin.enable);

  config = lib.mkIf config.catppuccin.enable {
    assets.catppuccin =
      lib.importJSON "${inputs.catppuccin-palette}/palette.json";
    sass.includePath = [
      (pkgs.runCommand "catppuccin-palette"
        {
          buildInputs = with pkgs; [
            nushell
          ];
        } ''
        mkdir $out

        nu -c '
          open ${inputs.catppuccin-palette}/palette.json
            | reject version
            | items {|name, palette|
              $palette.colors
                | items {|key, value| $"$($key): ($value.hex);"}
                | str join "\n"
                | save $"($env.out)/_($name).scss"
            }
        '
      '')
    ];
  };
}
