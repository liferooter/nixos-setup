{ pkgs, lib, config, ... }:
{
  config = lib.mkMerge [
    {
      console = {
        earlySetup = true;
        font = "${pkgs.terminus_font}/share/consolefonts/ter-k20n.psf.gz";
      };
    }
    (lib.mkIf (with config; catppuccin.enable && setup.isLaptop) {
      console.colors = map (color: (builtins.substring 1 6 config.assets.catppuccin.mocha.colors.${color}.hex)) [
        "base"
        "red"
        "green"
        "yellow"
        "blue"
        "pink"
        "teal"
        "subtext1"

        "surface2"
        "red"
        "green"
        "yellow"
        "blue"
        "pink"
        "teal"
        "subtext0"
      ];
    })
  ];
}
