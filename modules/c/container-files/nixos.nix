{ lib, config, ... }:
let
  cfg = config.setup.containerFiles;
in
{
  options = {
    setup.containerFiles = {
      root = lib.mkOption {
        description = "Path to the directory to store data for containers";
        type = with lib.types; str;
        default = "/var/lib/container-files";
      };
      containers = lib.mkOption {
        description = "List of subdirectories to store data for container";
        type = with lib.types; listOf str;
        default = [ ];
      };
      paths = lib.mkOption {
        description = "Paths of container file roots";
        type = with lib.types; attrsOf str;
        readOnly = true;
      };
    };
  };
  config = {
    setup.containerFiles.paths = lib.genAttrs cfg.containers (name: "${cfg.root}/${name}");
    systemd.tmpfiles.settings."container-files" = lib.mkMerge (lib.optional (cfg.containers != [ ])
      {
        ${cfg.root}.d = {
          user = "root";
          group = "root";
          mode = "0700";
        };
      } ++ lib.mapAttrsToList
      (_: dir: {
        ${dir}.d = {
          user = "1000";
          group = "1000";
        };
      })
      cfg.paths);
  };
}
