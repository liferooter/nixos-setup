{ inputs, ... }:
{
  imports = [
    inputs.nix-index-database.hmModules.nix-index
    ./backups.nix
  ];

  setup.system = "x86_64-linux";

  home.username = "liferooter";
  home.homeDirectory = "/home/liferooter";

  setup.keymap = {
    colemak.enable = true;
    useDefaultTweaks = true;
  };

  programs.git.enable = true;
  programs.fish.enable = true;

  setup.computerType = "server";
  home.stateVersion = "24.11";
}
