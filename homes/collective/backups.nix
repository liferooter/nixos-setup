{ pkgs
, lib
, ...
}:
{
  systemd.user.services.backup-minecraft = {
    Service.ExecStart = lib.getExe (pkgs.writeShellApplication {
      name = "backup-minecraft";

      runtimeInputs = with pkgs; [
        coreutils
        rdiff-backup
      ];

      text = ''
        mkdir -p ~/backups
        rdiff-backup backup \
          ~/liferooters-smp ~/backups
        rdiff-backup --force remove increments \
          --older-than 7D ~/backups || true
      '';
    });
  };
  systemd.user.timers.backup-minecraft = {
    Timer = {
      OnCalendar = "*-*-* *:00:00";
      Unit = "backup-minecraft.service";
    };
    Install.WantedBy = [ "default.target" ];
  };
}

