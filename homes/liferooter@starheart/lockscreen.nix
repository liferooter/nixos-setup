{ config, lib, pkgs, ... }:
{
  options = {
    setup.laptop.lockscreen = lib.mkPackageOption pkgs "lockscreen" {
      default = null;
    };
  };
  config =
    let
      pkg = config.setup.laptop.lockscreen;
    in
    {
      services.swayidle =
        let
          inherit (lib) getExe getExe';
          lockExe = getExe pkg;
          lockCmd = "${getExe' pkgs.systemd "systemd-run"} --user ${lockExe}";
        in
        {
          enable = true;
          events = [
            { event = "before-sleep"; command = lockCmd; }
            { event = "lock"; command = lockCmd; }
            { event = "unlock"; command = "pkill -f -USR1 ${lockExe}"; }
          ];
          timeouts = [
            { timeout = 15 * 60; command = lockCmd; }
            { timeout = 30 * 60; command = "${getExe' pkgs.systemd "systemctl"} suspend"; }
          ];
        };
      systemd.user.services.swayidle.Unit.After = [ "graphical-session.target" ];
    };
}
