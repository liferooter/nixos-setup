{ pkgs, inputs, util, lib, ... }:
let
  anyrunExt = name: "${pkgs.anyrun}/lib/lib${name}.so";
in
{
  imports = [
    inputs.anyrun.homeManagerModules.default
  ];

  programs.anyrun = {
    enable = true;
    package = pkgs.anyrun;
    config = {
      plugins = [
        (anyrunExt "applications")
        inputs.anyrun-files.packages.${pkgs.system}.default
        (anyrunExt "symbols")
        (anyrunExt "rink")
      ];
      x.fraction = 0.5;
      y.absolute = 0;
      width.absolute = 800;
      height.absolute = 0;
      hideIcons = false;
      ignoreExclusiveZones = false;
      layer = "overlay";
      hidePluginInfo = false;
      closeOnClick = true;
      showResultsImmediately = true;
      maxEntries = null;
    };
    extraConfigFiles."applications.ron".text =
      let
        terminalRunner = pkgs.writeShellApplication {
          name = "terminal-runner";
          runtimeInputs = with pkgs; [
            alacritty
            bash
          ];

          text = ''
            shift
            alacritty -e bash -c "$*"
          '';
        };
      in
      ''
        Config(
          max_entries: 5,
          desktop_actions: false,
          terminal: Some("${lib.getExe terminalRunner}"),
        )
      '';
    extraCss = builtins.readFile (util.compileSass {
      file = ./anyrun-style.scss;
    });
  };

}

