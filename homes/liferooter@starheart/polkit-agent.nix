{ pkgs, ... }:
{
  systemd.user.sessionServices = [
    { package = pkgs.hyprpolkitagent; binary = "/libexec/hyprpolkitagent"; }
  ];
}
