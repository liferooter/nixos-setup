{ pkgs, pkgs-unstable, ... }:
{
  imports = [
    ./niri.nix
    ./polkit-agent.nix
    ./lockscreen.nix
    ./hyprlock.nix
    ./theme.nix
    ./anyrun.nix
  ];

  wallpaper.file = ./wallpaper.png;

  programs.nethack.enable = true; #   For eating my lifetime
  programs.git.enable = true; #       For tracking malware files edited by @WeetHet
  programs.alacritty.enable = true; # For watching falling letters like in Matrix.

  programs.yeelight = {
    enable = true;
    defaultLamp = "shadow-sun";
  };

  services = {
    swaync.enable = true; #           For beautiful notifications
    playerctld.enable = true; #       For tracking playing music
  };

  home.packages =
    [
      pkgs.newsflash #                For reading news
      pkgs-unstable.thunderbird #     For reading mail
      pkgs.libreoffice #              Office suit
      pkgs.papers #                   For PDFs
      pkgs.simple-scan #              For scanning documents
    ] ++
    [
      pkgs-unstable.zed-editor #      For coding with @WeetHet
      pkgs.reuse #                    For licensing management
      pkgs.gnome-text-editor #        For opening text files in UI
    ] ++
    [
      pkgs.nixd #                     For coding in Nix easily
      pkgs.kubectl #                  For managing my Kubernetes
    ] ++
    [
      pkgs.obs-studio #               For screen recording
      pkgs.pavucontrol #              Volume controller
      pkgs.celluloid #                Player
    ] ++
    [
      pkgs.loupe #                    For images
      pkgs.nautilus #                 File manager
      pkgs.bluetuith #                For Bluetooth configuration
      pkgs.gnome-clocks #             For stopwatches and timers
    ];
}
