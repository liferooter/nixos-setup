{ config, assets, ... }:
let
  inherit (assets.catppuccin.mocha) colors;
  rgb = color: with color.rgb; "rgb(${toString r}, ${toString g}, ${toString b})";
in
{
  setup.laptop.lockscreen = config.programs.hyprlock.package;
  programs.hyprlock = {
    enable = true;
    settings = {
      general = {
        hide_cursor = true;
        no_fade_in = false;
      };

      background = [
        {
          path = "${config.wallpaper.file}";
          blur_passes = 3;
          blur_size = 8;
        }
      ];

      input-field = [
        {
          size = "200, 50";
          position = "0, 0";
          monitor = "";
          dots_center = true;
          fade_on_empty = false;
          font_color = rgb colors.text;
          inner_color = rgb colors.base;
          outer_color = rgb colors.crust;
          outline_thickness = 5;
          placeholder_text = ''<span foreground="#${colors.surface1.hex}">Password</span>'';
          shadow_passes = 1;
        }
      ];
    };
  };
}
