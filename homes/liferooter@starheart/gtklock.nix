{ pkgs, util, ... }:
let
  iniFormat = pkgs.formats.ini { };
in
{
  setup.laptop.lockscreen = pkgs.gtklock;
  xdg.configFile."gtklock/config.ini".source = iniFormat.generate "config.ini" {
    main.style = util.compileSass {
      file = ./gtklock.scss;
    };
  };
}
