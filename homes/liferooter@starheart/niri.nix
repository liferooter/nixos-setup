{ pkgs, lib, config, assets, inputs, ... }:
let
  inherit (lib) getExe getExe';
  mkKdl = import ./mkkdl.nix pkgs;
  colors = builtins.mapAttrs
    (_: color: color.hex)
    assets.catppuccin.mocha.colors;
in
{
  systemd.user.sessionServices = [
    { package = pkgs.xwayland-satellite; args = [ ":0" ]; }
    { package = pkgs.swaybg; args = [ "-i" config.wallpaper.file ]; }
    { package = inputs.scrollshell.packages.${pkgs.system}.default; }
  ];
  home.packages = [
    pkgs.niri
    (pkgs.writeScriptBin "default-user-session" ''
      #!${getExe pkgs.bash}

      exec ${getExe' pkgs.niri "niri-session"}
    '')
  ];
  xdg.configFile."niri/config.kdl".source = mkKdl "config.kdl"
    ({ block
     , section
     , node
     , ...
     }: [
      "prefer-no-csd"
      (block "hotkey-overlay" "skip-at-startup")
      (node "screenshot-path" "~/Pictures/Screenshots/Screenshot from %Y-%m-%d %H-%M-%S.png")
      (block "input" [
        (block "keyboard"
          (
            let xkb = config.home.keyboard; in
            block "xkb" {
              inherit (xkb) layout variant;
              options = lib.concatStringsSep "," xkb.options;
            }
          )
        )
        (block "touchpad" [
          "tap"
          "natural-scroll"
        ])
      ])
      (section "output" "eDP-1" {
        scale = 1.25;
      })
      (section "output" "GIGA-BYTE TECHNOLOGY CO., LTD. AORUS FI32U 22080B008688" {
        scale = 1.5;
      })
      (block "layout" [
        (node "gaps" 8)
        (node "center-focused-column" "never")
        "always-center-single-column"
        (block "preset-column-widths" [
          (node "proportion" 0.33333)
          (node "proportion" 0.5)
          (node "proportion" 0.66667)
        ])
        (block "default-column-width" {
          proportion = 0.5;
        })
        (block "focus-ring" [
          "off"
        ])
        (block "border" {
          width = 2;
          active-gradient = {
            from = colors.mauve;
            to = colors.blue;
            angle = 135;
          };
          inactive-color = colors.surface0;
        })
      ])
      (node "spawn-at-startup" (getExe pkgs.anyrun))
      (block "window-rule" {
        geometry-corner-radius = 2;
        clip-to-geometry = true;
        open-floating = false;
      })
      (block "binds" (
        let
          spawn = node "spawn";
          wpctl = getExe' pkgs.wireplumber "wpctl";
          brightnessctl = getExe pkgs.brightnessctl;
          cmds = lib.mapAttrs (_: getExe) pkgs;
          bindsWith = opts: lib.mapAttrsToList (key: section key opts);
          binds = bindsWith [ ];
        in
        lib.flatten [
          (binds {
            "Super+T" = spawn cmds.alacritty;
            "Super+slash" = spawn [ "${pkgs.swaynotificationcenter}/bin/swaync-client" "-t" ];
            "Super+Return" = spawn cmds.anyrun;
            "Super+Ctrl+L" = spawn (getExe config.setup.laptop.lockscreen);
            "Super+Q" = "close-window";
            "Super+Tab" = "focus-workspace-previous";
            "Super+Comma" = "consume-window-into-column";
            "Super+Period" = "expel-window-from-column";
            "Super+Shift+Q" = "quit";

            "Super+R" = "switch-preset-column-width";
            "Super+Shift+R" = "switch-preset-window-height";
            "Super+F" = "maximize-column";
            "Super+Shift+F" = "reset-window-height";
            "Super+Alt+F" = "fullscreen-window";
            "Super+C" = "center-column";

            "Print" = "screenshot-screen";
            "Shift+Print" = "screenshot";
            "Ctrl+Print" = "screenshot-window";
          })

          (bindsWith { allow-when-locked = true; } {
            XF86AudioRaiseVolume = spawn
              [ wpctl "set-volume" "@DEFAULT_AUDIO_SINK@" "5%+" "-l" "1.0" ];
            XF86AudioLowerVolume = spawn
              [ wpctl "set-volume" "@DEFAULT_AUDIO_SINK@" "5%-" ];
            XF86AudioMute = spawn
              [ wpctl "set-mute" "@DEFAULT_AUDIO_SINK@" "toggle" ];
            XF86MonBrightnessUp = spawn
              [ brightnessctl "set" "+5%" ];
            XF86MonBrightnessDown = spawn
              [ brightnessctl "set" "5%-" ];
          })

          (lib.forEach [
            { shortcut = "P"; key = "Play"; cmd = "play-pause"; }
            { shortcut = "Shift+Comma"; key = "Prev"; cmd = "previous"; }
            { shortcut = "Shift+Period"; key = "Next"; cmd = "next"; }
          ]
            ({ shortcut, key, cmd }:
              let
                action = spawn [ cmds.playerctl cmd ];
              in
              bindsWith { allow-when-locked = true; } {
                "Super+${shortcut}" = action;
                "XF86Audio${key}" = action;
              }))

          (lib.forEach [
            {
              item = "column";
              directions = [
                { keys = [ "Left" "N" ]; dir = "left"; }
                { keys = [ "Right" "O" ]; dir = "right"; }
              ];
            }
            {
              item = "window";
              directions = [
                { keys = [ "Down" "E" ]; dir = "down"; }
                { keys = [ "Up" "I" ]; dir = "up"; }
              ];
            }
          ]
            ({ item, directions }: lib.forEach directions
              ({ keys, dir }: lib.forEach keys (key: binds {
                "Super+${key}" = "focus-${item}-${dir}";
                "Super+Shift+${key}" = "move-${item}-${dir}";
                "Super+Ctrl+${key}" = "focus-monitor-${dir}";
                "Super+Shift+Ctrl+${key}" = "move-column-to-monitor-${dir}";
                "Super+Shift+Ctrl+Alt+${key}" = "move-workspace-to-monitor-${dir}";
              }))))

          (lib.forEach [
            { key = "Home"; pos = "first"; }
            { key = "End"; pos = "last"; }
          ]
            ({ key, pos }: binds {
              "Super+${key}" = "focus-column-${pos}";
              "Super+Shift+${key}" = "move-column-to-${pos}";
            }))

          (lib.forEach [
            { keys = [ "Page_Down" "Y" ]; dir = "down"; }
            { keys = [ "Page_Up" "U" ]; dir = "up"; }
          ]
            ({ keys, dir }: lib.forEach keys (key: binds {
              "Super+${key}" = "focus-workspace-${dir}";
              "Super+Shift+${key}" = "move-column-to-workspace-${dir}";
              "Super+Shift+Alt+${key}" = "move-workspace-${dir}";
            })))

          (lib.forEach [
            {
              item = { focus = "workspace"; move = "column-to-workspace"; };
              dirs = [ "Up" "Down" ];
            }
            {
              item = { focus = "column"; move = "column"; };
              dirs = [ "Left" "Right" ];
            }
          ]
            ({ item, dirs }: lib.forEach dirs (scrollDir:
              let
                dir = lib.toLower scrollDir;
              in
              bindsWith { cooldown-ms = 150; } {
                "Super+WheelScroll${scrollDir}" = "focus-${item.focus}-${dir}";
                "Super+Shift+WheelScroll${scrollDir}" = "move-${item.move}-${dir}";
              })))

          (lib.forEach (lib.range 1 9) (n: binds {
            "Super+${toString n}" = node "focus-workspace" n;
            "Super+Shift+${toString n}" = node "move-column-to-workspace" n;
            "Super+Shift+Ctrl+${toString n}" = node "move-window-to-workspace" n;
          }))

          (lib.forEach [ "Left" "Right" ] (dir: binds {
            "Super+Bracket${dir}" = "consume-or-expel-window-${lib.toLower dir}";
          }))

          (lib.forEach [
            { key = "Minus"; sign = "-"; }
            { key = "Equal"; sign = "+"; }
          ]
            ({ key, sign }: binds {
              "Mod+${key}" = node "set-column-width" "${sign}10%";
              "Mod+Shift+${key}" = node "set-window-height" "${sign}10%";
            }))
        ]
      ))
      (block "environment" {
        DISPLAY = ":0";
      })
    ]);
}
