{ pkgs, self, inputs, ... }:
{
  imports = [
    inputs.nix-index-database.hmModules.nix-index
  ];

  setup.system = "x86_64-linux";

  home.username = "liferooter";
  home.homeDirectory = "/var/home/liferooter";

  fonts.nerdFonts = [
    "JetBrainsMono"
  ];

  setup.keymap = {
    colemak.enable = true;
    useDefaultTweaks = true;
  };

  wallpaper.file = ./wallpaper.png;

  catppuccin.enable = true; #         For enjoying the beauty

  programs.nushell.enable = true; #   For working with tables
  programs.nethack.enable = true; #   For playing instead of work
  programs.git.enable = true; #       For tracking all I do
  services.gpg-agent = {
    enable = true; # For asking my password
    pinentryPackage = null;
    extraConfig = ''
      pinentry-program /usr/bin/pinentry
    '';
  };
  programs.yeelight = {
    enable = true;
    defaultLamp = "shadow-sun";
  };

  home.packages =
    [
      pkgs.reuse #              For licensing management
      pkgs.nixd #               For coding in Nix
      pkgs.nix-output-monitor # For building Nix derivations
    ]
    ++ (with self.packages.${pkgs.system}; [
      # For doing my homework
      hse-sans
      hse-slab
    ]);

  xdg.dataFile."mesa-drivers".source = pkgs.mesa.drivers;

  setup.computerType = "laptop";
  home.stateVersion = "24.11";
}
