{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.11";
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";

    home-manager = {
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    treefmt-nix = {
      url = "github:numtide/treefmt-nix";
      inputs.nixpkgs.follows = "nixpkgs-unstable";
    };

    nix-index-database = {
      url = "github:nix-community/nix-index-database";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    sops-nix = {
      url = "github:Mic92/sops-nix";
      inputs = {
        nixpkgs.follows = "nixpkgs-unstable";
      };
    };
    vscode-server = {
      url = "github:nix-community/nixos-vscode-server";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    yeelight = {
      url = "github:leixb/yeelight";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    tgpy = {
      url = "github:tm-a-t/tgpy/c4ce4d68a4a5db760df178bdf3065b957928e78d";
      inputs.nixpkgs.follows = "nixpkgs-unstable";
    };
    d-bot = {
      url = "gitlab:liferooter/d-bot";
      flake = false;
    };
    comment-keeper = {
      url = "gitlab:liferooter/comment-keeper";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    anyrun = {
      url = "github:anyrun-org/anyrun";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    anyrun-files = {
      url = "gitlab:liferooter/anyrun-files";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    scrollshell = {
      url = "gitlab:liferooter/scrollshell";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nethack-src = {
      url = "github:NetHack/NetHack/NetHack-3.7";
      flake = false;
    };
    tree-sitter-typst-src = {
      url = "github:uben0/tree-sitter-typst";
      flake = false;
    };
    any-sync-tools-src = {
      url = "github:anyproto/any-sync-tools/v0.2.8";
      flake = false;
    };

    nu-scripts = {
      url = "github:nushell/nu_scripts";
      flake = false;
    };

    catppuccin-alacritty = {
      url = "github:catppuccin/alacritty";
      flake = false;
    };
    catppuccin-helix = {
      url = "github:catppuccin/helix";
      flake = false;
    };
    catppuccin-tty = {
      url = "github:catppuccin/tty";
      flake = false;
    };
    catppuccin-palette = {
      url = "github:catppuccin/palette";
      flake = false;
    };
    catppuccin-swaync = {
      url = "github:catppuccin/swaync";
      flake = false;
    };
  };

  outputs =
    { self
    , nixpkgs
    , flake-parts
    , home-manager
    , ...
    }@inputs:
    let
      inherit (nixpkgs) lib;
      mkNixos = hostname:
        nixpkgs.lib.nixosSystem {
          modules = [
            ./modules.nix
            ./hosts/${hostname}
          ];

          specialArgs = {
            inherit self inputs hostname;
            systemConfig = null;
            mode = "nixos";
          };
        };
      mkHome = hostname: _:
        let
          nameComponents = lib.splitString "@" hostname;
          isStandalone = lib.length nameComponents == 1;
          homeConf = pkgs: home-manager.lib.homeManagerConfiguration {
            inherit lib pkgs;

            modules = [
              ./modules.nix
              {
                programs.home-manager.enable = true;
              }
              ./homes/${hostname}
            ];

            extraSpecialArgs = {
              inherit self inputs hostname;
              systemConfig = null;
              mode = "home";
            };
          };
          fakePkgs = {
            path = nixpkgs;
            overlays = [ ];
          };
          fakeHome = homeConf fakePkgs;
          pkgs = import nixpkgs {
            system = fakeHome.config.setup.system;
            config.allowUnfree = true;
          };
        in
        if isStandalone then { ${hostname} = homeConf pkgs; }
        else { };
    in
    flake-parts.lib.mkFlake { inherit inputs; }
      {
        imports = [
          inputs.treefmt-nix.flakeModule
          ./treefmt.nix
        ];

        systems = [ "x86_64-linux" ];

        flake.homeConfigurations = lib.concatMapAttrs mkHome (builtins.readDir ./homes);
        flake.nixosConfigurations = lib.genAttrs (lib.attrNames (builtins.readDir ./hosts)) mkNixos;

        flake.installerImages = lib.pipe self.nixosConfigurations [
          (lib.filterAttrs (_: cfg: cfg.config.setup.isServer))
          (lib.mapAttrs (hostname: cfg: nixpkgs.lib.nixosSystem {
            inherit (cfg.pkgs) system;

            modules = [
              ./installer
            ];

            specialArgs = {
              inherit self inputs hostname;

              serverConfig = cfg.config;
            };
          }))
          (lib.mapAttrs (_: cfg: cfg.config.system.build.isoImage))
        ];

        perSystem = { system, ... }: {
          _module.args = {
            pkgs = import nixpkgs {
              inherit system;
              config.allowUnfree = true;
            };
            sources = inputs;
          };

          imports = [
            ./packages
            ./shell.nix
          ];
        };
      };
}
