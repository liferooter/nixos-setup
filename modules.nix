{ lib, mode, ... }:
let
  inherit (builtins) readDir pathExists;
  inherit (lib) attrNames flatten map pipe filter hasPrefix;
  modulesDir = ./modules;
in
{
  imports = pipe modulesDir [
    readDir
    attrNames
    (map (letter:
      let
        dir = "${modulesDir}/${letter}";
      in
      pipe dir [
        readDir
        attrNames
        (map
          (subdir:
            if hasPrefix letter subdir
            then "${dir}/${subdir}"
            else abort "Module '${letter}/${subdir}' is not inside its directory"))
      ]))
    flatten
    (map (dir: map (mode: "${dir}/${mode}.nix") [ "generic" mode ]))
    flatten
    (filter pathExists)
  ];
}
