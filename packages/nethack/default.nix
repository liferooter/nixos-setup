{ src
, fetchzip
, stdenv
, ncurses
, util-linux
, groff
, lib
, gzip
, gnugrep
, bash
}:
stdenv.mkDerivation (finalAttrs: {
  pname = "nethack";
  version = "3.7-git";

  lua-version = "5.4.6";
  lua-tarball-hash = "sha256-K2dM3q69YuOhlKKE9qTcHFJUVhjlRPxQtKB8ZQOpAyE=";

  srcs = [
    src
    (fetchzip {
      name = "lua-src";
      url = "https://www.lua.org/ftp/lua-${finalAttrs.lua-version}.tar.gz";
      hash = finalAttrs.lua-tarball-hash;
    })
  ];

  sourceRoot = "source";

  postUnpack = ''
    mkdir -p source/lib/
    cp -r --no-preserve=mode lua-src source/lib/lua-${finalAttrs.lua-version}
  '';

  buildInputs = [ ncurses ];

  nativeBuildInputs = [
    util-linux
    groff
  ];

  postPatch = ''
    sed \
      -e '/^GIT_HASH/d' \
      -e '/^GIT_BRANCH/d' \
      -e '/^SHELLDIR/d' \
      -e '/-DCOMPRESS/s:/bin/gzip:${lib.getExe' gzip "gzip"}:' \
      -i sys/unix/hints/linux.370

    sed \
      -e '/^GDBPATH=/d' \
      -e '/^PANICTRACE_GDB=/s/1/0/' \
      -e '/^GREPPATH=/s:=.*:=${lib.getExe gnugrep}:' \
      -e '/^WIZARDS/s/=.*/=*/' \
      -i sys/unix/sysconf

    sed \
      -e 's/\bluckstone\b/laccstone/g' \
      -e 's/\bhiss\b/zHz/g' \
      -e 's/\bsouls\b/Russel Souls/g' \
      -i $(find -type f)
  '';

  patches = [ ./unixconf.patch ];

  configurePhase = ''
    sys/unix/setup.sh sys/unix/hints/linux.370
  '';

  postInstall = ''
    mkdir $out/bin
    cat > $out/bin/nethack << EOF
    #${lib.getExe bash}

    if [ -z "\$HOME" ]; then
      echo "Home directory must be set to play NetHack" >&2
      exit 1
    fi

    export NETHACKVARDIR="\$HOME/.local/share/nethack"

    if [ -e "\$NETHACKVARDIR" ]; then
      if [ ! -d "\$NETHACKVARDIR" ]; then
        echo "\$NETHACKVARDIR must not be something that is not a directory" >&2
        exit 1
      fi
    else
      mkdir -p "\$NETHACKVARDIR"
      cp -r $out/lib/nethack/vardir/* "\$NETHACKVARDIR"
      chmod -R +w "\$NETHACKVARDIR"/*
    fi

    $out/lib/nethack/nethack \$@
    EOF

    chmod +x $out/bin/nethack
  '';

  makeFlags = [
    "GIT=0"
    "PREFIX=$(out)"
    "HACKDIR=$(out)/lib/nethack"
    "VARDIR=$(out)/lib/nethack/vardir"
    "WANT_WIN_TTY=1"
    "WANT_WIN_CURSES=1"
  ];
})
