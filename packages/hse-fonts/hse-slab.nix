{ meta
, stdenvNoCC
, fetchzip
,
}:
stdenvNoCC.mkDerivation {
  pname = "hse-slab";
  version = "1.0.0";

  src = fetchzip {
    url = "https://www.hse.ru/mirror/pubs/share/533611928";
    hash = "sha256-RsITUMnyn3Kxwnse6e4UFsiVCZE4dFEBnRjHJcMpT+U=";
    stripRoot = false;
    extension = "zip";
  };

  installPhase = ''
    install -D *.otf -t $out/share/fonts/opentype
  '';

  inherit meta;
}
