{ meta
, stdenvNoCC
, fetchzip
,
}:
stdenvNoCC.mkDerivation {
  pname = "hse-sans";
  version = "1.0.0";

  src = fetchzip {
    url = "https://www.hse.ru/mirror/pubs/share/533611868";
    hash = "sha256-ic8TiitksxXl4pCUKwN2wb2IMCXZmLU7B3nYtHMVb4k=";
    stripRoot = false;
    extension = "zip";
  };

  installPhase = ''
    install -D *.otf -t $out/share/fonts/opentype
  '';

  inherit meta;
}
