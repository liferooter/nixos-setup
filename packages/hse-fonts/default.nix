{ lib, pkgs, ... }:
let
  meta = with lib; {
    homepage = "https://www.hse.ru/en/info/brandbook/";
    description = "HSE university font";
    license = licenses.unfree;
    /*
      Note that these fonts are unfree,
      I have permission to use them only because
      I'm a HSE student.
    */
  };
in
{
  packages.hse-sans = pkgs.callPackage ./hse-sans.nix { inherit meta; };
  packages.hse-slab = pkgs.callPackage ./hse-slab.nix { inherit meta; };
}
