{ pkgs, sources, ... }:
let
  inherit (pkgs) callPackage;
in
{
  imports = [ ./hse-fonts ];
  packages = {
    nethack = callPackage ./nethack {
      src = sources.nethack-src;
    };
    any-sync-tools = callPackage ./any-sync-tools {
      src = sources.any-sync-tools-src;
    };
  };
}
