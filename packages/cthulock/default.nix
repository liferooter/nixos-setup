{ src
, lib
, rustPlatform
, pkg-config
, libxkbcommon
, pam
, libGL
}:
let
  cargoToml = lib.importTOML "${src}/Cargo.toml";
  package = cargoToml.package;
in
rustPlatform.buildRustPackage {
  pname = package.name;

  inherit (package) version;
  inherit src;

  cargoLock.lockFile = "${src}/Cargo.lock";

  nativeBuildInputs = [
    rustPlatform.bindgenHook
    pkg-config
  ];

  buildInputs = [
    libxkbcommon
    pam
    libGL
  ];

  meta = {
    mainProgram = package.name;
    license = lib.licenses.gpl3Only;
  };
}
