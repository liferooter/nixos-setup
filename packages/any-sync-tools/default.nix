{ src
, lib
, buildGoModule
}:
let
  shareDir = "$out/share/any-sync-network";
  templatePath = "${shareDir}/defaultTemplate.yml";
in
buildGoModule {
  pname = "any-sync-tools";
  version = "0.2.8";

  inherit src;

  vendorHash = "sha256-Kfq+EV8r2w4hi271Vw4DqsWP4dYcTse3/aQcYbU9TDQ=";

  postPatch = ''
    substituteInPlace any-sync-network/cmd/root.go \
      --replace-fail ./defaultTemplate.yml ${templatePath}
  '';

  postInstall = ''
    install -D any-sync-network/defaultTemplate.yml \
      ${templatePath}
  '';

  meta = {
    description = "Configuration builder for Any-Sync nodes";
    homepage = "https://anytype.io";
    license = lib.licenses.mit;
  };
}
