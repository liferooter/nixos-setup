{ pkgs, ... }: {
  devShells.default = pkgs.mkShell {
    packages = with pkgs; [
      sops
      ssh-to-age
      nixd
      git
      nix-output-monitor
      nixpkgs-fmt
      nixos-rebuild
    ];
  };
}
