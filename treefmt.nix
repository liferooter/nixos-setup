{ ... }:
{
  perSystem = { ... }: {
    treefmt = {
      programs = {
        nixpkgs-fmt.enable = true;

        jsonfmt.enable = true;

        prettier.enable = true;
      };
      settings.excludes = [
        "*.png"
        "*.nu"
        "*.lock"
        "*.patch"
        "*.txt"

        "secrets/*"

        ".gitignore"
        ".envrc"
      ];
    };
  };
}
